# Python exercises


## Setup

You have several options to run those exercises.

### Python notebooks

Notebooks allow you to mix text and code, and the output from running it (even if the viewer doesn't have a running python environment or the modules that you used.)

- In [SWAN](https://swan.cern.ch) at CERN. Note that this requries a CERN account.
- In [Google Colab](https://colab.research.google.com/#create=true). You can create a new notebook using [https://colab.research.google.com/#create=true](https://colab.research.google.com/#create=true).
- Locally, after running the following commands, which will start a browser window (or tell you where to point your browser at to access). Make sure you have Python3 installed.

```
python3 -m venv venv
source ./venv/bin/activate
pip install jupyter
./venv/bin/jupyter notebook
```

### Python interpreter

You can also solve each problem set separately in a python file (extension `.py`) which you then execute with the Python interpreter `python` (or `python3` on some systems). Make sure you have Python version 3 running.


## Getting the notebooks locally

For the SWAN and local version, you need to clone the repostiory with the notebook.

### On SWAN

Click on the download from cloud icon [cloud-download](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAYAAADhu0ooAAAMZWlDQ1BJQ0MgUHJvZmlsZQAASImVlwdck0cbwO8dmSSsQARkhL0E2QSQEcKKICBTEJWQBBJGjAlBxU0tVbBuEcFR0aqIRasVkDoQsc6iuK2jOFCp1GIVFyrfZUCt/cbvu9/v3vvnueeee54nd+97B4BeN18mK0T1ASiSFsuTosNZkzIyWaQegAIDYATcgTVfoJBxEhPjACzD7d/Lq2sAUbWX3VS2/tn/X4uhUKQQAIBkQc4RKgRFkNsAwMsEMnkxAMQIKLedWSxTsRiykRw6CHmuivM0vFLFORrertZJSeJCbgGATOPz5XkA6HZAOatEkAft6D6E7CEVSqQA6BlBDhGI+ULIKZDHFBVNV/FCyE5QXwZ5F2R2zic28/5mP2fEPp+fN8KauNSFHCFRyAr5s//P1PzvUlSoHJ7DAVaaWB6TpIof5vBGwfRYFdMg90lz4hNUuYb8RiLU5B0AlCpWxqRq9FFzgYIL8weYkD2E/IhYyOaQo6SF8XFaeU6uJIoHGa4WdJakmJeiHbtEpIhM1trcKJ+elDDMuXIuRzu2kS9Xz6vS71AWpHK09m+IRbxh+y9LxSnpkKkAYNQSSVo8ZF3IRoqC5FiNDmZTKubGD+vIlUkq/+0gs0XS6HCNfSwrVx6VpNWXFSmG48XKxRJevJari8UpMZr8YLsFfLX/JpCbRFJO6rAdkWJS3HAsQlFEpCZ2rFMkTdXGi92VFYcnacf2ywoTtfo4WVQYrZLbQDZTlCRrx+LjiuHi1NjH42TFiSkaP/HsfP74RI0/eAmIA1wQAVhACWsOmA7ygaSzr7kP/tL0RAE+kIM8IAJuWsnwiHR1jxQ+k0Ep+B2SCChGxoWre0WgBMo/jEg1TzeQq+4tUY8oAI8gF4FYUAh/K9WjpCOzpYGHUCL5x+wC6GshrKq+f8o4UBKnlSiH7bL0hjWJkcQIYgwxiuiMm+EheBAeB59hsHrhbDxg2Nu/9AmPCF2E+4SrhG7CzWmSMvlnvkwA3dB+lDbinE8jxh2gTV88HA+G1qFlnImbATfcB87DwUPhzL5QytX6rYqd9W/iHIngk5xr9SgeFJQyihJGcfp8pK6Lru+IFVVGP82PxteckaxyR3o+n5/7SZ6FsI39XBNbgh3ATmHHsTPYYawZsLBjWAt2Hjui4pE19FC9hoZnS1L7UwDtSP4xH187pyqTCo8Gj16P99o+UCyaVazaYNzpstlySZ64mMWBXwERiycVuI9heXl4eQKg+qZoXlMvmOpvBcI8+5es7BAAwbyhoaHDf8lifwTgwDK4za//JXPKge/iMQCcrhEo5SUaGa56EODbQA/uKFNgCWyBE4zIC/iBIBAGIsF4kABSQAaYCvMshutZDmaCuWARKAeVYCVYB2rAFrAN7ALfgf2gGRwGx8FP4By4CK6CW3D99ICnoB+8AoMIgpAQOsJATBErxB5xRbwQNhKCRCJxSBKSgWQjeYgUUSJzkS+QSmQ1UoNsReqR75FDyHHkDNKF3ETuIb3In8g7FENpqBFqgTqgY1E2ykFj0RR0CpqHzkBL0cXocrQarUP3oE3ocfQcehXtRp+iAxjAdDAmZo25YWyMiyVgmVguJsfmYxVYFVaHNWKt8J++jHVjfdhbnIgzcBbuBtdwDJ6KC/AZ+Hx8GV6D78Kb8A78Mn4P78c/EugEc4IrIZDAI0wi5BFmEsoJVYQdhIOEk3A39RBeEYlEJtGR6A93YwYxnziHuIy4ibiX2EbsIj4gDpBIJFOSKymYlEDik4pJ5aQNpD2kY6RLpB7SG7IO2YrsRY4iZ5Kl5DJyFXk3+Sj5EvkxeZCiT7GnBFISKELKbMoKynZKK+UCpYcySDWgOlKDqSnUfOoiajW1kXqSepv6QkdHx0YnQGeijkRnoU61zj6d0zr3dN7SDGkuNC4ti6akLaftpLXRbtJe0Ol0B3oYPZNeTF9Or6efoN+lv9Fl6Lrr8nSFugt0a3WbdC/pPtOj6NnrcfSm6pXqVekd0Lug16dP0XfQ5+rz9efr1+of0r+uP2DAMPA0SDAoMlhmsNvgjMETQ5Khg2GkodBwseE2wxOGDxgYw5bBZQgYXzC2M04yeoyIRo5GPKN8o0qj74w6jfqNDY19jNOMZxnXGh8x7mZiTAcmj1nIXMHcz7zGfDfKYhRnlGjU0lGNoy6Nem0y2iTMRGRSYbLX5KrJO1OWaaRpgekq02bTO2a4mYvZRLOZZpvNTpr1jTYaHTRaMLpi9P7Rv5ij5i7mSeZzzLeZnzcfsLC0iLaQWWywOGHRZ8m0DLPMt1xredSy14phFWIlsVprdczqN5Yxi8MqZFWzOlj91ubWMdZK663WndaDNo42qTZlNntt7thSbdm2ubZrbdtt++2s7CbYzbVrsPvFnmLPthfbr7c/Zf/awdEh3eErh2aHJ44mjjzHUscGx9tOdKdQpxlOdU5XnInObOcC503OF11QF18XsUutywVX1NXPVeK6ybVrDGFMwBjpmLox191obhy3ErcGt3vuTPc49zL3ZvdnY+3GZo5dNfbU2I8evh6FHts9bnkaeo73LPNs9fzTy8VL4FXrdcWb7h3lvcC7xfu5j6uPyGezzw1fhu8E3698230/+Pn7yf0a/Xr97fyz/Tf6X2cbsRPZy9inAwgB4QELAg4HvA30CywO3B/4R5BbUEHQ7qAn4xzHicZtH/cg2CaYH7w1uDuEFZId8k1Id6h1KD+0LvR+mG2YMGxH2GOOMyefs4fzLNwjXB5+MPw1N5A7j9sWgUVER1REdEYaRqZG1kTejbKJyotqiOqP9o2eE90WQ4iJjVkVc51nwRPw6nn94/3HzxvfEUuLTY6tib0f5xInj2udgE4YP2HNhNvx9vHS+OYEkMBLWJNwJ9ExcUbijxOJExMn1k58lOSZNDfpVDIjeVry7uRXKeEpK1JupTqlKlPb0/TSstLq016nR6SvTu+eNHbSvEnnMswyJBktmaTMtMwdmQOTIyevm9yT5ZtVnnVtiuOUWVPOTDWbWjj1yDS9afxpB7IJ2enZu7Pf8xP4dfyBHF7Oxpx+AVewXvBUGCZcK+wVBYtWix7nBueuzn2SF5y3Jq9XHCquEvdJuJIayfP8mPwt+a8LEgp2FgwVphfuLSIXZRcdkhpKC6Qd0y2nz5reJXOVlcu6ZwTOWDejXx4r36FAFFMULcVG8PB+Xumk/FJ5rySkpLbkzcy0mQdmGcySzjo/22X20tmPS6NKv52DzxHMaZ9rPXfR3HvzOPO2zkfm58xvX2C7YPGCnoXRC3ctoi4qWPRzmUfZ6rKXX6R/0brYYvHCxQ++jP6yoVy3XF5+/augr7YswZdIlnQu9V66YenHCmHF2UqPyqrK98sEy85+7fl19ddDy3OXd67wW7F5JXGldOW1VaGrdq02WF26+sGaCWua1rLWVqx9uW7aujNVPlVb1lPXK9d3V8dVt2yw27Byw/sacc3V2vDavRvNNy7d+HqTcNOlzWGbG7dYbKnc8u4byTc3tkZvbapzqKvaRtxWsu3R9rTtp75lf1u/w2xH5Y4PO6U7u3cl7eqo96+v322+e0UD2qBs6N2TtefidxHftTS6NW7dy9xbuQ/sU+777fvs76/tj93ffoB9oPEH+x82HmQcrGhCmmY39TeLm7tbMlq6Do0/1N4a1HrwR/cfdx62Plx7xPjIiqPUo4uPDh0rPTbQJmvrO553/EH7tPZbJyaduNIxsaPzZOzJ0z9F/XTiFOfUsdPBpw+fCTxz6Cz7bPM5v3NN533PH/zZ9+eDnX6dTRf8L7RcDLjY2jWu6+il0EvHL0dc/ukK78q5q/FXu66lXrtxPet69w3hjSc3C28+/6Xkl8FbC28Tblfc0b9Tddf8bt2vzr/u7fbrPnIv4t75+8n3bz0QPHj6UPHwfc/iR/RHVY+tHtc/8XpyuDeq9+Jvk3/reSp7OthX/rvB7xufOT374Y+wP873T+rveS5/PvTnshemL3a+9HnZPpA4cPdV0avB1xVvTN/sest+e+pd+rvHgzPfk95Xf3D+0Pox9uPtoaKhIRlfzlcfBTBY0dxcAP7cCQA9AwDGRXh+mKy586kLormnqgn8J9bcC9XFD4BG2KiO69w2APbB6tCmvpIA1VE9JQyg3t4jVVsUud5eGls0eOMhvBkaemEBAKkVgA/yoaHBTUNDH+AdFbsJQNsMzV1TVYjwbvBNsIqumggXgs+K5h76SYyft0DlgQ/4vP0XHgOI6PbuUgsAAABcZVhJZk1NACoAAAAIAAQBBgADAAAAAQACAAABEgADAAAAAQABAAABKAADAAAAAQACAACHaQAEAAAAAQAAAD4AAAAAAAKgAgAEAAAAAQAAADqgAwAEAAAAAQAAADoAAAAAccUEXQAAArRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPHRpZmY6UmVzb2x1dGlvblVuaXQ+MjwvdGlmZjpSZXNvbHV0aW9uVW5pdD4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgICAgPHRpZmY6Q29tcHJlc3Npb24+MTwvdGlmZjpDb21wcmVzc2lvbj4KICAgICAgICAgPHRpZmY6UGhvdG9tZXRyaWNJbnRlcnByZXRhdGlvbj4yPC90aWZmOlBob3RvbWV0cmljSW50ZXJwcmV0YXRpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWURpbWVuc2lvbj41ODwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj41ODwvZXhpZjpQaXhlbFhEaW1lbnNpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgpyaDIUAAALxUlEQVRoBd2beWxU1xXGD+PxbmO8gA0YbGMoXtghbCEVCIVCMY2iLKSVUhQ1apq2ahoatZUSRZEapanUojTN8kcrpVWVJqEhVGIJkAJhX2PALMbgDcxiAzZe8L7Q87vjNx2Px/absQ1OPuQ3b73vfO+ee+655xyG3VXIAKO5pV2On6+QU0U3peDybblUXiflVfVSfadZGlvbzNvCg50yIipUkuIiJSUpWiaPj5Xp6SNlTkaihIYEDbBEIsMGimhtfYtsOlAsXxy7JLtPXZFAP9+wYSJLpifLww+kyKoHJ8jwyJABId1vomeKbsk/d5yXj3cXdCGXnRInaaNjJCk+QuJjwiUmIsT8hYcFG8Ebm1qlpqHF/FXWNEp5ZYOUXK+Rs5eq3MQg/dSSyfL0sgyZkp7gPh/ITsBEL16plnc2nJSN+4vc752fkaQCxUv2+HgZMyrKqGZ4qFOCnQ5xBjnMr8Oh0is6Ou5Ka1uHtLV3mN/G5jaj2tdu3JGzlyvlTFGlHD5f7m770UXp8vPHZsik5BHuc/7sBER03SdfyVufnnS/Z9X8NFkwdYxk6jiLGx4m0dp7EHQqQYd2i+Gmv/SQbs1zd0X/YR10o5z17660KXEI12pP365tknwd34dOX5NNh0vc7/rl4zNk7erZ7mO7O34RRU1f+dshyS28YdpfPidFFs9KlqkTEiQxLkKiwoO114KEXoMgxPwBxCHs6u12qWtolRu3GySv+Jbsyb0i245fMs3NmjhKXn92gV/qbJvoht0XZe37e00vTNSxt0pV6SHtxWRV0eFqPUODlWAA5Hr6EBbp5tZ2qVVrXXajTvblXZfNB4qkUMcyH3Hd89+Wx5ZM6qmJLudtEX1v4yl581/HzYMPzxov352fKtMmjdSpIUIiVEUdDoffvddFil4ODOGODmlQlS6vrJcTF2/K9iOX5Ivcy+ap3/5gjvz00em9tOC65OzrDs/x+OTiSbJMzX5mapwkqCVlvqMXBxM0H6SGLFKHRfKoaH2nU2IiQyU2OlTW77loOqCppa3PcdsrUXrSMjrPfCdTls9LlXS1ekz0jMVB5tjl+/FBw5QkWsQwwR5EhDnl79vzjYxc661neyTKmLTU9ZnlmbJqYbqkj42RaJ3AgwZRVbuw8zrgw/KB42PCJMQZJyHBriHzwbZ8I2viiIgex6zDqy1ziHXF8ADUdfm8NEMSL4X58F72pBHCY8O7kQFZJo4dYWRbvfhb5g5kRnZf8EmUKQQjsGy2Gh5VVyZpGmasDBUgCzIh24p5KUZWZEZ2X+gmOcaHedJMIQsnSIYanhgdk1jWoQZkQjZkzFmQZmRGdjh4o4v0uHWW8clRhzp7Qrx6OuH3XV29hbaOLTVGxmz1hXO0YwAc4OKJLkTxXQEqu3DKaBkVG+Ee8J4PDaV9yGKUElXWBdlJRnbks7hYsrqJMogtB33RtDEyTucszPdgz5OWIP35RUZkTU6MlgVTkkxTcPE0TG6iLLXASjU++K7MlUwj/QHuW17hTfl01wXZefyylFyr6U9zvT6LrLHRYTJNF+8r56aaey1OHJh5lEUz60kwNytJxiRE6eQcuEPA+vKVvx6UbboIb2dp4oFZE0fKC4/PlCWzx3mc7f8uKozMyD5HOWw5Wmo4vfzDucY6my4jMoBpnqvryUxdMOMUBGpl6bmlL34mW46UdiMJnVzt4TVv7pA/ftTdMvaXLr3KlJOlHOZNTjSc4AYMUcIfIDstzkQDQnQdyRfyF0VXq+X5dbukqq6pz0ff/uykvLU+t8/7/LkBmXEPE9RzytLhByxuTgJZxHhAxrg4Ga6L5kAcA1R07Tt7pUnHpV28+588eUSXe8cLbshBXWCHhQbJkpnjZNncFLtNdLsP2aPV6c8YF2uuwQ2OTqJ1qC0xHqJxgVraL3PL5ISqpT/AWC1/aaNGBv//cT78b4Gs0QXE755d6E9T7nuxwJFqgcfpOjl7fJyGZapMRNJBSBKkJcUYLyPQVcnpkkr3y7x3FmQmyd63n5B9f3nC/HJswZOkde4fuiLZd+qqdejXL+oLh1gN6aSNHm6ehaOTuCtISojULxGsU0oAg1OfP9cL0dHadmrnS3kXx31hrxJ9aPrYvm7zeR0OBASS1AIDODoILoOEEbr0UQ8jEJ6Mz5LrtaadgdpERbjCooG0B4eQTqPE83B0EkEHrNpZ/vhrbglx/OxPu6TgikszTGMDsFmqIZuAofoLlxidagAcHaQJAAHmYL2IjtvFzepGefLVLbLzRJndR2zf9+v398m/dfEfCFzjVOfUcBdRODqtXEhEaLAJU1pxVzsv+I0KQyjSFxaqd0JsF2Smxne5heOlnR+4qbldDpy73uU6B2dKK+VX7+2VY/nl8vufLPLLdsCBcerOCmi+xx1K8TfXhN/aU0/ykjeeWyQTxsR0I8CJ57431fyxX6ztLH1xg08viusf775gElA/ypnCYcBwkNUCDZoLwajwzw5O6ZzJ/OsLtPP917b26cTzsbiP+3vDn3V92aSTvl3AgTbJ7wA4OlilgNrGFpMD6Ul4c5PHpkLHZ2+4rhH2p3ohC0muc19fqK5vlotl9o0dHFo0vVHd4LI/cHSQnwTVuoIh4dNjN3lJk67R+r7QE1l/SFrvIHhtFwxD8jg1d1w9CkcHbh+orGmSFnXF+tAi97uy07oaGPcFrx1vsoGQpEniyXbh6tF2qax1aR0cHWSaQfmtO1Lf1NbneLFehndDhNAOLLIb9xTaVlfPdkdpVsDTs/K85msfzWxobDUpDK7D0UE6HZSU12pXN+s4bbervfLGjx+UBA1M2QFkX3hnj60x6d0e77Eb0qE3ybuyVCzt9Nbg6KBmgAmWTPPlijq1vm0mdef9Ml/H5EI/enWFWfn4ut7fc0xTL62e5deyrYOElFrbsgoSylWGGxwdJIqoGQDny6pMErYdo2QTk1NiZdMfHpGXn54rS2cmS6KqWX/AR0/VwFyOJpc3vr5KfqFhF3/QpkaG0FDBFVeJANzgaCZRCiN2nbwiZ0uq1Cg1yuj4SL+SSPiVnk6AP4IN5L3GCKlBrSRbrlwA3IAJpVD9wZc8qjUD+Zdu6xdp1qyz/V41LQ2BTbvKXKdlAfk6DI8UVBhOcAOGKAElqj/AUfU7r9+qN54IX+jrAmTFe7p2s16OnXMVecAJbsAQZYcSF0D07rQ66nj8fKGvC5C1uq5Z8jSaQKgTWJzYdxOljocSF7A/75qpGfDHApsH79OGAg9kLauolYNnXCshuHjWJrmJIiN1PGDHV5fNA1SEtLR22J5XzcP3eOMyQB1SobIeOltuZEcEi4slThei5Bqp4wGbNfB7trjS1PvgaQzF8YpMyFalrt45lXXzwWIjOxy8C6+6EOUuipWo46HEZZM+WKCTLh7TULTCyIQtOV9aZWRFZmT3VXDVjShkKVZiukGFtxwqkaKrNepIuNarXB8KwKnBMSjUPOhWNaDIiszI7gs+iTKIKVYCn3x5UT4/UiLFmm6gYV5wP9WYd1sk6YDtSnK9ygiQ2dMAmZOdm6DXFJ4nrP0sXYaRx9ivqQIi8BGaLhgRFaaR/GATSsTJ5gveS1hj8raq6wWN1W5VbftAg92Awqo1K7J6FKdHojzxgEbUWcQe1gn4ZOEtE/MlN2NVbAYFQfbesGUKIYdC5BEjif34cKcr1Ynx6csn9rtEjrT/SnW46fFErcUluo+vO1h8rV6s1xUJUQZIft45JukMuyVytojSoHfR44r5qTI/e7RZEMdpppkk7EDWBEIQq9qovVilTnqpWlQ0a9vh0sEreoQooCbAs4zVKuqYoQWQY0ZGS7SWrZEKGIgyVsI6dRoluHazTgsdSSuWuwsdB7WM1UXVtfUshORMjlaWUZicpWtTsljWOPa3MJnIAIXJrECqNIblKky+KptVVS3ck8Jk62X89lZqPoVS88Qok8+xDBfjmJJzogaqlZ3Fx91Lza9qlOOchjbRnvteau5JGIG+0f95wJMs+zgT3+j/DuJNmOOh+B98/gfL5dPoXx8ppAAAAABJRU5ErkJggg==) and enter the URL to this repository:

```
https://gitlab.cern.ch/karolos-potamianos/esipap/Python-Exercises.git
```

### Locally

Clone the repository

```
git clone https://gitlab.cern.ch/karolos-potamianos/esipap/Python-Exercises.git

```


Change to your project, set up the virtualenv and then open your editor to create new `.py` files.


```
cd Python-Exercises
python3 -m venv venv
source ./venv/bin/activate
```


If you'd like to use a notebook, start the Jupyter notebook (after running the commands above).

```
./venv/bin/pip install jupyter
./venv/bin/jupyter notebook
```

You should have a browser window opening with the notebook file in it.

### Google Colab

A copy of the notebooks is available in this folder:

[https://drive.google.com/drive/u/0/folders/1QxPmi-dcFvGOGbmE11sH3T9qBDqcDad6](https://drive.google.com/drive/u/0/folders/1QxPmi-dcFvGOGbmE11sH3T9qBDqcDad6)
